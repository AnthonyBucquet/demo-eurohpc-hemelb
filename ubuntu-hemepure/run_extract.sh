#!/bin/bash

# $1 : Input directory
# $2 : Input file
# $3 : Output directory

source  /opt/spack-environment/activate.sh

/opt/hemeXtract/hemeXtract -X -i $1 -o ${1}_tmp.txt
/opt/hemeXtract/paraviewProcessing.sh ${1}_tmp.txt /data/${2}_paraview
rm -f ${1}_tmp.txt