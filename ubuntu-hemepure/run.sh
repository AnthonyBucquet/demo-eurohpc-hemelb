#!/bin/bash

# $1 : Input directory
# $2 : Input file
# $3 : Output directory

source  /opt/spack-environment/activate.sh

cd $1

#OMPI_MCA_pml=ucx UCX_TLS=sm,tcp OMPI_MCA_pml_ucx_tls=sm,tcp

mpirun --hostfile /etc/JARVICE/nodes --mca pml ucx --mca btl ^openib --mca io ompio --mca fs_ufs_lock_algorithm 3 -x PATH -x LD_LIBRARY_PATH hemepure -in $2 -out /data/$3

#cd /data/$3/Extracted
#hemeXtract  -X whole.dat > readable-output.txt
#bash /opt/paraviewProcessing.sh readable-output.txt paraview-file-name
