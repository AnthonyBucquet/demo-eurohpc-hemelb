# Demo Eurohpc Hemelb



## Getting started

To get started:

```
cd ubuntu-hemepure
docker build -t ubuntu-hemepure:latest_x86_64 .
```

## Authors and acknowledgment

- David Guibert
- Anthony Bucquet

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
